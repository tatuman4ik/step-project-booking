package menu;

public enum MenuCommands {

    LOGIN("LogIn"),
    LOGOUT("LogOut."),
    REGISTER("Register"),
    VIEW_TIMETABLE("View Timetable"),
    VIEW_FLIGHT("View Flight"),
    SEARCH_AND_BOOK_FLIGHT("Search & Book Flight"),
    VIEW_BOOKING("View Booking"),
    CANCEL_BOOKING("Cancel Booking"),
    HELP("Help"),
    EXIT("Exit");

    private final String description;

    private MenuCommands(String description){
        this.description = description;
    }

    public  String getDescription(){
        return this.description;
    }

}
