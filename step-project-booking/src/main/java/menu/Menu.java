package menu;

import libs.Console;
import libs.ConsoleColor;

import java.util.List;

import static menu.MenuCommands.*;

public class Menu {

    private List<MenuCommands> menuCommands;

    private Menu(){}

    public static Menu menuForRegisterUser(){
        Menu menu = new Menu();
        menu.menuCommands = List.of(
                VIEW_TIMETABLE,
                VIEW_FLIGHT,
                SEARCH_AND_BOOK_FLIGHT,
                VIEW_BOOKING,
                CANCEL_BOOKING,
                HELP,
                LOGOUT);
        return menu;
    }

    public static Menu menuForNewUser(){
        Menu menu = new Menu();
        menu.menuCommands = List.of(
                LOGIN,
                REGISTER,
                VIEW_TIMETABLE,
                VIEW_FLIGHT,
                HELP,
                EXIT);
        return menu;
    }

    public void printMenu(){
        Console.println(lineToString());
        Console.println(headerToString());
        Console.println(lineToString());

        for (int i = 0; i < menuCommands.size(); i++) {
            Console.println(menuItemToString(i, menuCommands.get(i)));
        }
        Console.println(lineToString());
    }

    private String lineToString(){
        return "=".repeat(60);
    }
    private String headerToString(){
        return "\t".repeat(5)+ConsoleColor.CYAN+ "FLIGHT BOOKING MANAGER"+ConsoleColor.RESET;
    }
    private String menuItemToString(int i, MenuCommands item){
        return "\t".repeat(5)+String.format("%d. %s",++i,item.getDescription());
    }

    public boolean isNotValidIndex(String stringLine) {
        boolean isValid = false;
        try {
            int indexMenu = Integer.parseInt(stringLine);
            isValid = (indexMenu > 0 && indexMenu <= this.menuCommands.size());
        }catch (NumberFormatException ex){

        }
        return !isValid;
    }

    public MenuCommands getMenuCommandsByIndex(int index){
        return menuCommands.get(index-1);
    }

}
