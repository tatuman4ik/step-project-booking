package entity;

import java.io.Serializable;

public class Place implements Serializable {
    public int row;
    public Seat seat;
    public boolean vacant = true;

    public Place(int row, Seat seat) {
        this.row = row;
        this.seat = seat;
    }

    public int getRow() {
        return row;
    }

    public Seat getSeat() {
        return seat;
    }

    public boolean isVacant() {
        return vacant;
    }

    public boolean bookPlace() {
        if (vacant) {
            vacant = false;
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        return String.format("%d%s", row, seat);
    }

    public boolean setVacant() {
        return this.vacant = true;
    }
}
