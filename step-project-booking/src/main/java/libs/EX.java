package libs;

public class EX {
    public static final RuntimeException NI = new RuntimeException("not implemented yet");
    public static final RuntimeException ECREATF = new RuntimeException("Error while creating file");
    public static final RuntimeException EREADF = new RuntimeException("Error while reading data form file");
    public static final RuntimeException EGETIND = new RuntimeException("Error while getting Flight by index");
    public static final RuntimeException EWRITEF = new RuntimeException("Error while loading data to file");

}
