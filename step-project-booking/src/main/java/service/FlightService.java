package service;

import dao.*;
import entity.*;
import libs.*;

import java.time.*;
import java.util.*;
import java.util.stream.*;

public class FlightService {
    private DAO<Flight> flightDao;

    public FlightService() {
        flightDao = new FlightDao();
    }

    public List<Flight> getAllFlights() {
        return flightDao.getAll();
    }

    public void displayAllFamilies() {
        List<Flight> flights = flightDao.getAll();
        IntStream.range(0, flights.size())
                .mapToObj(i -> (i + 1) + ". " + flights.get(i).prettyFormat())
                .forEach(Console::println);
    }

    public Flight createNewFlight(Airline airline, City departure, City destination, LocalDate date, LocalTime time, int capacity) {
        int id = flightDao.getAll().size();
        if (id > 0) {
            id = flightDao.getAll()
                    .stream()
                    .mapToInt(Flight::id)
                    .max()
                    .orElse(0);
        }
        Flight newFlight = new Flight(airline, departure, destination, date, time, capacity, ++id);
        flightDao.save(newFlight);
        return newFlight;
    }

    public boolean deleteFlightByIndex(int index) {
        return flightDao.delete(index);
    }

    public boolean placeReservation(int flightIndex, Place place) {
        Flight flight = getAllFlights().get(flightIndex);
        boolean reserved = flight.getBoard().bookPlace(place.getRow(), place.getSeat());
        flightDao.save(flight);
        return reserved;
    }

    public boolean cancellation(int flightIndex, Place place) {
        Flight flight = getAllFlights().get(flightIndex);
        boolean cancelled = flight.getBoard().cancellation(place.getRow(), place.getSeat());
        flightDao.save(flight);
        return cancelled;
    }

    public int countFlights() { return flightDao.getAll().size(); }

    public Flight getFlightByIndex(int index) { return flightDao.getByIndex(index); }

    public int vacantCounter(int index) { return flightDao.getByIndex(index).getBoard().countVacantSeats(); }

    public void showVacantPlaces(int index) { Console.println(flightDao.getByIndex(index).getBoard().showVacantSeats()); }

    public List<Flight> getFlightsFrom(City departure) {
        return flightDao.getAll()
                .stream()
                .filter(flight -> flight.getDeparture().equals(departure))
                .toList();
    }

    public List<Flight> getFlightsTo(City destination) {
        return flightDao.getAll()
                .stream()
                .filter(flight -> flight.getDestination().equals(destination))
                .toList();
    }

    public List<Flight> getFlightsFromTo(City departure, City destination) {
        return flightDao.getAll()
                .stream()
                .filter(flight -> flight.getDeparture().equals(departure))
                .filter(flight -> flight.getDestination().equals(destination))
                .toList();
    }

    public List<Flight> getFlightsFromToDate(City departure, City destination, LocalDate date) {
        return flightDao.getAll()
                .stream()
                .filter(flight -> flight.getDeparture().equals(departure))
                .filter(flight -> flight.getDestination().equals(destination))
                .filter(flight -> flight.getFlightDate().equals(date))
                .toList();
    }
}
